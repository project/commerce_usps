<?php

namespace Drupal\commerce_usps\Plugin\Commerce\ShippingMethod;

use Drupal\commerce_shipping\Entity\ShipmentInterface;

/**
 * Provides the USPS shipping method.
 *
 * The "_9xxx" ids represent various First-Class Mail services that
 * share the same service id of 0 from USPS api (sad!).
 *
 * @see \Drupal\commerce_usps\USPSRateRequest::resolveRates().
 *
 * @CommerceShippingMethod(
 *  id = "usps",
 *  label = @Translation("USPS"),
 *  services = {
 *    "_0" = @Translation("First-Class Mail Large Envelope"),
 *    "_1" = @Translation("Priority Mail"),
 *    "_1058" = @Translation("USPS Ground Advantage"),
 *    "_1096" = @Translation("USPS Ground Advantage Cubic"),
 *    "_13" = @Translation("Priority Mail Express 2-Day Flat Rate Envelope"),
 *    "_16" = @Translation("Priority Mail Flat Rate Envelope"),
 *    "_17" = @Translation("Priority Mail Medium Flat Rate Box"),
 *    "_2058" = @Translation("USPS Ground Advantage Hold For Pickup"),
 *    "_2096" = @Translation("USPS Ground Advantage Cubic Hold For Pickup"),
 *    "_22" = @Translation("Priority Mail Large Flat Rate Box"),
 *    "_27" = @Translation("Priority Mail Express 2-Day Flat Rate Envelope Hold For Pickup"),
 *    "_28" = @Translation("Priority Mail Small Flat Rate Box"),
 *    "_29" = @Translation("Priority Mail Padded Flat Rate Envelope"),
 *    "_3" = @Translation("Priority Mail Express 2-Day Hold For Pickup"),
 *    "_30" = @Translation("Priority Mail Express 2-Day Legal Flat Rate Envelope"),
 *    "_31" = @Translation("Priority Mail Express 2-Day Legal Flat Rate Envelope Hold For Pickup"),
 *    "_33" = @Translation("Priority Mail Hold For Pickup"),
 *    "_34" = @Translation("Priority Mail Large Flat Rate Box Hold For Pickup"),
 *    "_35" = @Translation("Priority Mail Medium Flat Rate Box Hold For Pickup"),
 *    "_36" = @Translation("Priority Mail Small Flat Rate Box Hold For Pickup"),
 *    "_37" = @Translation("Priority Mail Flat Rate Envelope Hold For Pickup"),
 *    "_38" = @Translation("Priority Mail Gift Card Flat Rate Envelope"),
 *    "_39" = @Translation("Priority Mail Gift Card Flat Rate Envelope Hold For Pickup"),
 *    "_40" = @Translation("Priority Mail Window Flat Rate Envelope"),
 *    "_4001" = @Translation("Priority Mail Express 2-Day HAZMAT"),
 *    "_4010" = @Translation("Priority Mail HAZMAT"),
 *    "_4012" = @Translation("Priority Mail Large Flat Rate Box HAZMAT"),
 *    "_4013" = @Translation("Priority Mail Medium Flat Rate Box HAZMAT"),
 *    "_4014" = @Translation("Priority Mail Small Flat Rate Box HAZMAT"),
 *    "_4058" = @Translation("USPS Ground Advantage HAZMAT"),
 *    "_4096" = @Translation("USPS Ground Advantage Cubic HAZMAT"),
 *    "_41" = @Translation("Priority Mail Window Flat Rate Envelope Hold For Pickup"),
 *    "_42" = @Translation("Priority Mail Small Flat Rate Envelope"),
 *    "_43" = @Translation("Priority Mail Small Flat Rate Envelope Hold For Pickup"),
 *    "_44" = @Translation("Priority Mail Legal Flat Rate Envelope"),
 *    "_45" = @Translation("Priority Mail Legal Flat Rate Envelope Hold For Pickup"),
 *    "_46" = @Translation("Priority Mail Padded Flat Rate Envelope Hold For Pickup"),
 *    "_6001" = @Translation("Priority Mail Express 2-Day Parcel Locker"),
 *    "_6010" = @Translation("Priority Mail Parcel Locker"),
 *    "_6012" = @Translation("Priority Mail Large Flat Rate Box Parcel Locker"),
 *    "_6013" = @Translation("Priority Mail Medium Flat Rate Box Parcel Locker"),
 *    "_6014" = @Translation("Priority Mail Small Flat Rate Box Parcel Locker"),
 *    "_6058" = @Translation("USPS Ground Advantage Parcel Locker"),
 *    "_6075" = @Translation("Library Mail Parcel Parcel Locker"),
 *    "_6076" = @Translation("Media Mail Parcel Parcel Locker"),
 *    "_6096" = @Translation("USPS Ground Advantage Cubic Parcel Locker"),
 *    "_62" = @Translation("Priority Mail Express 2-Day Padded Flat Rate Envelope"),
 *    "_63" = @Translation("Priority Mail Express 2-Day Padded Flat Rate Envelope Hold For Pickup"),
 *  }
 * )
 */
class USPS extends USPSBase {

  /**
   * {@inheritdoc}
   */
  public function calculateRates(ShipmentInterface $shipment) {
    // Only attempt to collect rates if an address exists on the shipment.
    if ($shipment->getShippingProfile()->get('address')->isEmpty()) {
      return [];
    }

    // Only attempt to collect rates for US addresses.
    if ($shipment->getShippingProfile()->get('address')->country_code != 'US') {
      return [];
    }

    // Make sure a package type is set on the shipment.
    $this->setPackageType($shipment);

    return $this->uspsRateService->getRates($shipment, $this->parentEntity);
  }

}
